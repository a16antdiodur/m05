package iam.dam.deDios.Calculadora;

public class DivisioEntreZeroException extends Exception {
	public DivisioEntreZeroException () {
		super();
	}
	
	public DivisioEntreZeroException (String message) {
		super (message);
	}
}