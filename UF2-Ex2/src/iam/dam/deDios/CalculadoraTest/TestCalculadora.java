package iam.dam.deDios.CalculadoraTest;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import iam.dam.deDios.Calculadora.Calculadora;
import iam.dam.deDios.Calculadora.DivisioEntreZeroException;

public class TestCalculadora {

	private Calculadora calculadora;

	@Before
	public void setUp() {
		System.out.println("@Before");
		this.calculadora = new Calculadora();
	}

	@After
	public void tearDown() {
		System.out.println("@After");
		this.calculadora = null;
	}

	@Test
	public void testSumar() {
		assertTrue(calculadora.sumar(2, 6) == 8);
	}

	@Test
	public void testRestar() {
		assertTrue(calculadora.restar(5, 2) == 3);
	}

	@Test
	public void testMultiplicar() {
		assertTrue(calculadora.multiplicar(5, 2) == 10);
	}

	@Test
	public void testDividir() throws DivisioEntreZeroException {
		assertTrue(calculadora.dividir(10, 2) == 5);
	}
	
	@Test(expected = DivisioEntreZeroException.class)
	public void testDividirZero() throws DivisioEntreZeroException {
		calculadora.dividir(10,  0);
		fail("Error no es pot divir entre zero.");
	}
}
